/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookmvc.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Address {
	private int addressId;
	
	@NotEmpty(message = "Please enter a first name.")
	private String firstName;
	
	@NotEmpty(message = "Please enter a last name.")
	private String lastName;
	
	@NotEmpty(message = "Please enter an address.")
	private String address;
	
	@NotEmpty(message = "Please enter a phone number.")
	@Length(max = 10, message = "Phone numbers cannot be longer than 10 characters.")
	private String phone;
	
	@NotEmpty(message = "Please enter an email address.")
	@Email(message = "Please enter a valid email address.")
	private String email;
	
	public Address() {
		
	}
	
	public Address(String firstName, String lastName, String address, String phone, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phone = phone;
		this.email = email;
	}
	
	public Address(int addressId, String firstName, String lastName, String address, String phone, String email) {
		this.addressId = addressId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phone = phone;
		this.email = email;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
