/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookmvc.dao;

import com.sg.addressbookmvc.dto.Address;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoInMemImpl implements AddressBookDao {
	private final Map<Integer, Address> addressBook;
	private static int addressBookIdCounter = 0;

	public AddressBookDaoInMemImpl() {
		addressBook = new HashMap<>();
		
		Address a1 = new Address();
		
		a1.setAddressId(addressBookIdCounter);
		a1.setFirstName("Jay");
		a1.setLastName("Pavlinak");
		a1.setAddress("401 South Main Street");
		a1.setPhone("4405551212");
		a1.setEmail("hooverbunyip@yahoo.com");
		
		addressBook.put(addressBookIdCounter, a1);
		addressBookIdCounter++;
		
		Address a2 = new Address();
		
		a2.setAddressId(addressBookIdCounter);
		a2.setFirstName("Morgan");
		a2.setLastName("Chesterland");
		a2.setAddress("123 Broken Dreams Boulevard");
		a2.setPhone("3305551212");
		a2.setEmail("fake_email@nowhere.com");
		
		addressBook.put(addressBookIdCounter, a2);
		addressBookIdCounter++;
	}
	
	@Override
	public Address createAddress(Address address) {
		address.setAddressId(addressBookIdCounter);
		addressBookIdCounter++;
		
		addressBook.put(address.getAddressId(), address);
		
		return address;
	}

	@Override
	public Address getAddressById(int id) {
		return addressBook.get(id);
	}

	@Override
	public List<Address> getAllAddresses() {
		return new ArrayList<>(addressBook.values());
	}

	@Override
	public void updateAddress(Address address) {
		addressBook.put(address.getAddressId(), address);
	}

	@Override
	public void deleteAddress(int id) {
		addressBook.remove(id);
	}

	@Override
	public List<Address> searchAddresses(Map<SearchTerm, String> criteria) {
		
		String firstNameCriteria = criteria.get(SearchTerm.FIRST_NAME);
		String lastNameCriteria = criteria.get(SearchTerm.LAST_NAME);
		String addressCriteria = criteria.get(SearchTerm.ADDRESS);
		String phoneCriteria = criteria.get(SearchTerm.PHONE);
		String emailCriteria = criteria.get(SearchTerm.EMAIL);
		
		Predicate<Address> firstNameMatches;
		Predicate<Address> lastNameMatches;
		Predicate<Address> addressMatches;
		Predicate<Address> phoneMatches;
		Predicate<Address> emailMatches;
		
		Predicate<Address> truePredicate = (address) -> { return true; };
		
		firstNameMatches = (firstNameCriteria == null || firstNameCriteria.isEmpty() ? truePredicate : (address) -> address.getFirstName().equalsIgnoreCase(firstNameCriteria));
		lastNameMatches = (lastNameCriteria == null || lastNameCriteria.isEmpty() ? truePredicate : (address) -> address.getLastName().equalsIgnoreCase(lastNameCriteria));
		addressMatches = (addressCriteria == null || addressCriteria.isEmpty() ? truePredicate : (address) -> address.getAddress().equalsIgnoreCase(addressCriteria));
		phoneMatches = (phoneCriteria == null || phoneCriteria.isEmpty() ? truePredicate : (address) -> address.getPhone().equalsIgnoreCase(phoneCriteria));
		emailMatches = (emailCriteria == null || emailCriteria.isEmpty() ? truePredicate : (address) -> address.getEmail().equalsIgnoreCase(emailCriteria));
		
		return addressBook.values().stream().filter(firstNameMatches.and(lastNameMatches).and(addressMatches).and(phoneMatches).and(emailMatches)).collect(Collectors.toList());
	}
	
}
