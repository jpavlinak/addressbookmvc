/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookmvc.dao;

import com.sg.addressbookmvc.dto.Address;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface AddressBookDao {
	
	public Address createAddress(Address address);
	public Address getAddressById(int id);
	public List<Address> getAllAddresses();
	public void updateAddress(Address address);
	public void deleteAddress(int id);
	
	public List<Address> searchAddresses(Map<SearchTerm, String> criteria);
}
