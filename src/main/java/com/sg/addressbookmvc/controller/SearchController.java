/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookmvc.controller;

import com.sg.addressbookmvc.dao.AddressBookDao;
import com.sg.addressbookmvc.dao.SearchTerm;
import com.sg.addressbookmvc.dto.Address;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
public class SearchController {
	
	private AddressBookDao dao;
	
	@Inject
	public SearchController(AddressBookDao dao) {
		this.dao = dao;
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String displaySearchPage() {
		return "search";
	}
	
	@RequestMapping(value = "search/addresses", method = RequestMethod.POST)
	@ResponseBody
	public List<Address> searchAddresses(@RequestBody Map<String, String> searchMap) {
		Map<SearchTerm, String> criteriaMap = new HashMap<>();
		
		String term = searchMap.get("firstName");
		if(!term.isEmpty()) {
			criteriaMap.put(SearchTerm.FIRST_NAME, term);
		}
		
		term = searchMap.get("lastName");
		if(!term.isEmpty()) {
			criteriaMap.put(SearchTerm.LAST_NAME, term);
		}
		
		term = searchMap.get("address");
		if(!term.isEmpty()) {
			criteriaMap.put(SearchTerm.ADDRESS, term);
		}
		
		term = searchMap.get("phone");
		if(!term.isEmpty()) {
			criteriaMap.put(SearchTerm.PHONE, term);
		}
		
		term = searchMap.get("email");
		if(!term.isEmpty()) {
			criteriaMap.put(SearchTerm.EMAIL, term);
		}
		
		return dao.searchAddresses(criteriaMap);
	}
}
