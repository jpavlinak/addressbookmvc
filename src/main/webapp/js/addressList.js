/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
	loadAddresses();
	
	$("#add-button").click(function (event) {
		event.preventDefault();
		
		$.ajax({
			
			type: "POST",
			url: "address",
			
			data: JSON.stringify({
				firstName: $("#add-first-name").val(),
				lastName: $("#add-last-name").val(),
				phone: $("#add-phone").val(),
				email: $("#add-email").val(),
				address: $("#add-address").val()
			}),
			
			contentType: "application/json; charset=utf-8",
			
			headers: {
				"Accept": "application/json",
				"Content-Type": "application/json"
			},
			
			dataType: "json"
		}).success(function (data, status) {
			$("#add-first-name").val("");
			$("#add-last-name").val("");
			$("#add-phone").val("");
			$("#add-email").val("");
			$("#add-address").val("");
			
			$("#validationErrors").empty();
			loadAddresses();
		}).error(function (data, status) {
			$("#validationErrors").empty();
			
			$.each(data.responseJSON.fieldErrors, function (index, validationError) {
				var errorDiv = $("#validationErrors");
				errorDiv.append(validationError.message).append($("<br>"));
			});
		});
	});
	
	$("#edit-button").click(function (event) {
		event.preventDefault();
		
		$.ajax({
			
			type: "PUT",
			url: "address/" + $("#edit-id").val(),
			
			data: JSON.stringify({
				firstName: $("#edit-first-name").val(),
				lastName: $("#edit-last-name").val(),
				phone: $("#edit-phone").val(),
				email: $("#edit-email").val(),
				address: $("#edit-address").val()
			}),
			
			contentType: "application/json; charset=utf-8",
			
			headers: {
				"Accept" : "application/json",
				"Content-Type" : "application/json"
			},
			
			dataType: "json"
		}).success(function () {
			loadAddresses();
		});
	});
	
	$("#search-button").click(function (event) {
		event.preventDefault();
		
		$.ajax({
			
			type: "POST",
			url: "search/addresses",
			
			data: JSON.stringify({
				firstName: $("#search-first-name").val(),
				lastName: $("#search-last-name").val(),
				phone: $("#search-phone").val(),
				email: $("#search-email").val(),
				address: $("#search-address").val()
			}),
			
			headers: {
				"Accept": "application/json",
				"Content-Type": "application/json"
			},
			
			dataType: "json"
		}).success(function (data, status) {
			$("#search-first-name").val("");
			$("#search-last-name").val("");
			$("#search-phone").val("");
			$("#search-email").val("");
			$("#search-address").val("");
				
			fillAddressTable(data, status);
		});
	});
});

function loadAddresses() {
	$.ajax({
		url: "addresses"
	}).success(function (data, status) {
		fillAddressTable(data, status);
	});
}

function fillAddressTable(addressList, status) {
	clearAddressTable();
	
	var summaryTable = $("#addressRows");
	
	$.each(addressList, function (index, address) {
		summaryTable.append($("<tr>")
				.append($("<td>")
					.append($("<a>").attr({"data-address-id" : address.addressId,
										   "data-toggle" : "modal",
										   "data-target" : "#detailsModal"})
									.text((index) + " - " + address.firstName + " " + address.lastName)))
				.append($("<td>").text(address.phone))
				.append($("<td>").text(address.email))
				.append($("<td>")
					.append($("<a>").attr({"data-address-id" : address.addressId,
										   "data-toggle" : "modal",
										   "data-target" : "#editModal"})
									.text("Edit")))
				.append($("<td>")
					.append($("<a>").attr({"onClick" : "deleteAddress(" + address.addressId + ")"}).text("Delete"))));
	});
}

function clearAddressTable() {
	$("#addressRows").empty();
}

$("#detailsModal").on("show.bs.modal", function (event) {
	var element = $(event.relatedTarget);
	var addressId = element.data("address-id");
	var modal = $(this);
	
	$.ajax({
		type: "GET",
		url: "address/" + addressId
	}).success(function (sampleAddress) {
		modal.find("#address-id").text(sampleAddress.addressId);
		modal.find("#address-name").text(sampleAddress.firstName + " " + sampleAddress.lastName);
		modal.find("#address-phone").text(sampleAddress.phone);
		modal.find("#address-email").text(sampleAddress.email);
		modal.find("#address-address").text(sampleAddress.address);
	});
});

$("#editModal").on("show.bs.modal", function (event) {
	var element = $(event.relatedTarget);
	var addressId = element.data("address-id");
	var modal = $(this);
	
	$.ajax({
		type: "GET",
		url: "address/" + addressId
	}).success(function (sampleEditAddress) {
		modal.find("#edit-id").val(sampleEditAddress.addressId);
		modal.find("#edit-first-name").val(sampleEditAddress.firstName);
		modal.find("#edit-last-name").val(sampleEditAddress.lastName);
		modal.find("#edit-phone").val(sampleEditAddress.phone);
		modal.find("#edit-email").val(sampleEditAddress.email);
		modal.find("#edit-address").val(sampleEditAddress.address);
	});
});

function deleteAddress(id) {
	var answer = confirm("Are you sure you want do delete this Address?");
	
	if(answer === true) {
		
		$.ajax({
			
			type: "DELETE",
			url: "address/" + id
			
		}).success(function () {
			loadAddresses();
		});
	}
}