<%-- 
    Document   : home
    Created on : Nov 3, 2016, 1:44:29 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Address Book Home</title>
    </head>
    <body>
		<div class="container">
			<h1>Address Book Home</h1>

			<hr>

			<div class="navbar">

				<ul class="nav nav-tabs">

					<li role="presentation" class="active">
						<a href="${pageContext.request.contextPath}/home">Home</a>
					</li>

					<li role="presentation">
						<a href="${pageContext.request.contextPath}/search">Search</a>
					</li>

					<li role="presentation">
						<a href="${pageContext.request.contextPath}/stats">Stats</a>
					</li>

				</ul>
			</div>

			<div class="row">

				<div class="col-md-6">

					<table id="addressTable" class="table table-hover table-striped">

						<tr>
							<th>Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th></th>
							<th></th>
						</tr>

						<tbody id="addressRows"></tbody>

					</table>
				</div>

				<div class="col-md-6">

					<div id="validationErrors" class="warning bg-danger"></div>

					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="add-first-name" class="col-xs-4 control-label">First Name:</label>

							<div class="col-xs-8">
								<input type="text" id="add-first-name" class="form-control" name="firstName" placeholder="First Name" required>
							</div>
						</div>

						<div class="form-group">
							<label for="add-last-name" class="col-xs-4 control-label">Last Name:</label>

							<div class="col-xs-8">
								<input type="text" id="add-last-name" class="form-control" name="lastName" placeholder="Last Name" required>
							</div>
						</div>

						<div class="form-group">
							<label for="add-phone" class="col-xs-4 control-label">Phone:</label>

							<div class="col-xs-8">
								<input type="text" id="add-phone" class="form-control" name="phone" placeholder="Phone" required>
							</div>
						</div>

						<div class="form-group">
							<label for="add-emial" class="col-xs-4 control-label">Email:</label>

							<div class="col-xs-8">
								<input type="text" id="add-email" class="form-control" name="phone" placeholder="Email" required>
							</div>
						</div>

						<div class="form-group">
							<label for="add-address" class="col-xs-4 control-label">Address:</label>

							<div class="col-xs-8">
								<input type="text" id="add-address" class="form-control" name="address" placeholder="Address" required>
							</div>
						</div>

						<div class="form-group">

							<div class="col-xs-offset-4 col-xs-8">
								<button type="submit" id="add-button" class="btn btn-primary">Add Address</button>
							</div>

						</div>

					</form>

				</div>
			</div>

			<div class="modal face" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">

				<div class="modal-dialog">

					<div class="modal-content">

						<div class="modal-header">

							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Close</span>
							</button>

							<h4 class="modal-title" id="detailsModalLabel">Address Details</h4>

						</div>

						<div class="modal-body">
							<h3 id="addressId"></h3>

							<table class="table table-bordered">

								<tr>
									<th>Name:</th>
									<td id="address-name"></td>
								</tr>

								<tr>
									<th>Phone:</th>
									<td id="address-phone"></td>
								</tr>

								<tr>
									<th>Email:</th>
									<td id="address-email"></td>
								</tr>

								<tr>
									<th>Address:</th>
									<td id="address-address"></td>
								</tr>

							</table>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>

					</div>
				</div>
			</div>

			<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editDetailsModalLabel" aria-hidden="true">

				<div class="modal-dialog">

					<div class="modal-content">

						<div class="modal-header">

							<button type="button" class="close" data-dismass="modal">
								<span aria-hidden="true">&times</span>
								<span class="sr-only">Close</span>
							</button>

							<h4 class="modal-title" id="editDetailsModalLabel">Edit Address</h4>

						</div>

						<div class="modal-body">

							<h2>Edit Address</h2>

							<form class="form-horizontal" role="form">

								<div class="form-group">
									<label for="edit-first-name" class="col-xs-4 control-label">First Name:</label>

									<div class="col-xs-8">
										<input type="text" id="edit-first-name" class="form-control" placeholder="First Name">
									</div>
								</div>

								<div class="form-group">
									<label for="edit-last-name" class="col-xs-4 control-label">Last Name:</label>

									<div class="col-xs-8">
										<input type="text" id="edit-last-name" class="form-control" placeholder="Last Name">
									</div>
								</div>

								<div class="form-group">
									<label for="edit-phone" class="col-xs-4 control-label">Phone:</label>

									<div class="col-xs-8">
										<input type="text" id="edit-phone" class="form-control" placeholder="Phone">
									</div>
								</div>

								<div class="form-group">
									<label for="edit-email" class="col-xs-4 control-label">Email:</label>

									<div class="col-xs-8">
										<input type="text" id="edit-email" class="form-control" placeholder="Email">
									</div>
								</div>

								<div class="form-group">
									<label for="edit-address" class="col-xs-4 control-label">Address:</label>

									<div class="col-xs-8">
										<input type="text" id="edit-address" class="form-control" placeholder="Address">
									</div>
								</div>

							</form>

						</div>

						<div class="modal-footer">
							<button type="submit" id="edit-button" class="btn btn-primary" data-dismiss="modal">Edit</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>

							<input type="hidden" id="edit-id">
						</div>

					</div>
				</div>
			</div>

		</div>

		<script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/addressList.js"></script>

    </body>
</html>
