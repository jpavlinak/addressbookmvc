<%-- 
    Document   : stats
    Created on : Nov 3, 2016, 1:45:14 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
		
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
		<div class="container">
			<h1>Address Stats Page!</h1>
			
			<hr>
			
			<div class="navbar">
				
				<ul class="nav nav-tabs">
					
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/home">Home</a>
					</li>
					
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/search">Search</a>
					</li>
					
					<li role="presentation" class="active">
						<a href="${pageContext.request.contextPath}/stats">Stats</a>
					</li>
				</ul>
				
			</div>
		</div>
		
		<script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		
    </body>
</html>
